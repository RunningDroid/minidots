# shellcheck shell=bash
# shellcheck disable=SC2155,SC1090,SC1091
HISTFILE=~/.zsh/histfile
HISTSIZE=20000
# shellcheck disable=SC2034
SAVEHIST=20000
bindkey -v

zstyle ':completion:*' use-cache on
zstyle ':completion:*' cache-path ~/.zsh/cache

zstyle ':completion:*' completer _expand _complete _ignored _match _prefix
zstyle ':completion:*' completions 1
zstyle ':completion:*' expand suffix
zstyle ':completion:*' format '%d'
zstyle ':completion:*' glob 1
zstyle ':completion:*' group-name ''
zstyle ':completion:*' ignore-parents parent pwd .. directory
zstyle ':completion:*' insert-unambiguous true
zstyle ':completion:*' list-colors ''
zstyle ':completion:*' list-prompt %SAt %p: Hit TAB for more, or the character to insert%s
zstyle ':completion:*' list-suffixes true
zstyle ':completion:*' matcher-list '+' 'm:{[:lower:]}={[:upper:]}' 'r:|[._-]=** r:|=**'
zstyle ':completion:*' menu select=1
zstyle ':completion:*' original true
zstyle ':completion:*' select-prompt %SScrolling active: current selection at %p%s
zstyle ':completion:*' squeeze-slashes true
zstyle ':completion:*' substitute 1
zstyle :compinstall filename "$HOME/.zshrc"

autoload -Uz compinit add-zsh-hook up-line-or-beginning-search down-line-or-beginning-search
fpath[1,0]=~/.zsh/completions
compinit -d "$HOME/.cache/zcompdump"
{
  # Compile the completion dump to increase speed of next startup. Run in background.
  zcompdump="$HOME/.cache/zcompdump"
  if [[ -s "$zcompdump" && (! -s "${zcompdump}.zwc" || "$zcompdump" -nt "${zcompdump}.zwc") ]]; then
	# if zcompdump file exists, and we don't have a compiled version or the
	# dump file is newer than the compiled file
	zcompile "$zcompdump"
  fi
} &!

if [[ -e /etc/zsh/zprofile ]]; then
    source /etc/zsh/zprofile
fi

source "$HOME"/.zsh/aliases

source "$HOME"/.zsh/functions

zle -N up-line-or-beginning-search
zle -N down-line-or-beginning-search
source "$HOME"/.zsh/keybinds

if [[ -n "$SSH_CONNECTION" ]]; then
	source "$HOME"/.zshenv
fi

# Sun Apr 17 08:40:54 PM EDT 2022: I don't use this on any of my machines
source "$HOME"/.zsh/local

local antigen_dir="${HOME}/.cache/antigen"
if ! [[ -d "$antigen_dir" ]]; then
	mkdir -p "$antigen_dir"
fi

if ! [[ -d "${antigen_dir}/repo" ]]; then
	git clone 'https://github.com/zsh-users/antigen.git' "${antigen_dir}/repo"
fi

export ADOTDIR="${antigen_dir}/dotdir"

# load antigen
source "${antigen_dir}/repo/antigen.zsh"

# automatically run antigen reset if antigenrc has changed
# TODO: check if comparing mtime in shell would be faster
# NOTE: there's no difference in performance between sha256 & md5
if ! [[ -e "${antigen_dir}/antigenrc.hash" ]]; then
	sha256sum "${HOME}/.zsh/antigenrc" > "${antigen_dir}/antigenrc.hash"
elif ! sha256sum -c "${antigen_dir}/antigenrc.hash" > /dev/null; then
	antigen reset
	sha256sum "${HOME}/.zsh/antigenrc" > "${antigen_dir}/antigenrc.hash"
fi

antigen init "${HOME}/.zsh/antigenrc"

export GPG_TTY=$(tty)
if [ -f "/tmp/.gpg-agent.info" ]; then
    source "/tmp/gpg-agent.info"
fi

if ( [[ $COLORTERM =~ ^(truecolor|24bit)$ ]] || [[ $TERM = alacritty ]] ); then
	local hostname_hash=$(hostname | tr -d '\n' | sha1sum - | tr '[:lower:]' '[:upper:]')

	local red_hex=$(echo "$hostname_hash" | cut -b 1,2)
	local green_hex=$(echo "$hostname_hash" | cut -b 3,4)
	local blue_hex=$(echo "$hostname_hash" | cut -b 5,6)
	colored_hostname="%F{#${red_hex}${green_hex}${blue_hex}}%m"
else
	colored_hostname='%F{white}%m'
fi

PROMPT="%(0?.%F{green}(%?%).%F{red}(%?%))%F{red}($colored_hostname%F{red})%f
%F{red}(%b%F{green}%~%F{red})%(! %F{red}#%f %F{green}$%f)%f"
unset colored_hostname


# history options
setopt share_history hist_save_no_dups hist_ignore_space hist_lex_words hist_reduce_blanks hist_fcntl_lock
# I/O options
setopt interactive_comments hash_cmds hash_dirs hash_executables_only

setopt extended_glob no_match
unsetopt auto_cd auto_pushd beep notify

# add bookmarks
# ~prefixdir
if [ -e "${HOME}/.local/share/wineprefixes" ]; then
	hash -d prefixdir="${HOME}/.local/share/wineprefixes"
fi
# ~steamapps
if [ -e "${HOME}/.local/share/Steam/" ]; then
	# my hacky multi-profile stuff means I can't check for the existence of the full path
	hash -d steamapps="${HOME}/.local/share/Steam/steamapps/common/"
fi

if command -v direnv >/dev/null; then
    eval "$(direnv hook zsh)"
fi

if command -v hass-cli >/dev/null; then
	source <(_HASS_CLI_COMPLETE=zsh_source hass-cli)
fi

# terminfo/termcap based terminal title
function title_precmd () {
	if tput hs; then
		local tsl="$(tput tsl)"
		local fsl="$(tput fsl)"
		# Keep $tsl and $fsl separate from the prompt escapes so that ZSH will
		#	ALWAYS print the value of the variable instead of the name
		print -n "${tsl}"
		print -Pn '%m %1~'
		print -n "${fsl}"
	fi
}

function title_preexec () {
	if tput hs; then
		local tsl="$(tput tsl)"
		local fsl="$(tput fsl)"
		# Keep $tsl and $fsl separate from the prompt escapes so that ZSH will
		#	ALWAYS print the value of the variable instead of the name
		print -n "${tsl}"
		print -Pn '%m %1~ %# '
		# print the command without -P so zsh doesn't try to parse it
		print -n "${(q)1}${fsl}"
	fi
}

if tput hs; then
	add-zsh-hook -Uz precmd title_precmd
	add-zsh-hook -Uz preexec title_preexec
fi

# tell foot the value of $PWD
function osc7-pwd() {
    emulate -L zsh # also sets localoptions for us
    setopt extendedglob
    local LC_ALL=C
    printf '\e]7;file://%s%s\e\' $HOST ${PWD//(#m)([^@-Za-z&-;_~])/%${(l:2::0:)$(([##16]#MATCH))}}
}

function chpwd-osc7-pwd() {
    (( ZSH_SUBSHELL )) || osc7-pwd
}
add-zsh-hook -Uz chpwd chpwd-osc7-pwd

# enable jumping between prompts in foot
function osc133-precmd() {
    print -Pn "\e]133;A\e\\"
}

add-zsh-hook -Uz precmd osc133-precmd

ttyctl -f

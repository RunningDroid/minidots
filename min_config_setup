#!/bin/sh -eu

# Inspired by this HN comment: https://news.ycombinator.com/item?id=11071754
# Archive links: https://archive.vn/FBqXd	https://web.archive.org/web/20190523164227/https://news.ycombinator.com/item?id=11071754

# to use this do 'curl -Lso min_config_setup.sh https://gitlab.com/RunningDroid/minidots/raw/master/min_config_setup && chmod +x min_config_setup.sh && ./min_config_setup.sh'

#rm dein
if [ -e "$HOME/.cache/dein" ]
then
	rm -rf "$HOME/.cache/dein"
fi

(
	# the posix utilities are assumed to be GNU, but I've never tested non-GNU versions
	# separated by '' (Unit Separator (0x1F) usually rendered as '^_') (Ctrl-v Ctrl-Shift--)
	required_non_posix_utilities="gitcurlzshnvimssh"

	# also Unit Separator (GitLab doesn't render control characters)
	IFS=''
	for utility in ${required_non_posix_utilities}; do
		if ! command -v "$utility" > /dev/null ; then
			printf 'Error: "%s" command not found\n' "$utility"
			exit 2
		fi
	done
)

if ! [ -e "$HOME/.ssh/id_rsa-gitlab" ]; then
	echo 'Generate an ssh key for gitlab and add it to gitlab before running this script'
	echo "Generate an ssh key with 'ssh-keygen -o -t rsa -C \"$(uname --nodename)\" -b 4096 -f .ssh/id_rsa-gitlab'"
	exit 3
fi

export GIT_SSH_COMMAND="ssh -i $HOME/.ssh/id_rsa-gitlab"
alias min_config='git --git-dir=$HOME/.local/share/min_config.git --work-tree=$HOME'

if ! [ -e "$HOME/.local/share/min_config.git" ]; then
	git clone --bare git@gitlab.com:RunningDroid/minidots.git "$HOME/.local/share/min_config.git"
fi

# if min_config_setup (without the .sh) doesn't exist then we're either running for the first time or checkout failed last time
if ! [ -e "$HOME/min_config_setup" ]; then
	min_config checkout
fi

min_config config --local status.showUntrackedFiles no

# setup device-local config (not actually used)
touch ~/.zsh/local
# setup zgen's paths
zsh -ic 'zgen save'
# make neovim regen the plugin manifest
nvim --headless +UpdateRemotePlugins +qall
# make vim-plug update? (might not work because the bit that downloads vim-plug might not have fired)
# NOTE: if this doesn't work then run 'nvim +qall' first
nvim -c 'PlugUpdate' +qall

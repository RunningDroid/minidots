# .zshenv
# shellcheck shell=bash
# shellcheck disable=SC2155,SC1090,SC1091

# [[ XDG Base Directory Specification 0.6
if [ -n "$XDG_CONFIG_HOME" ]; then
	export CONF_DIR="$(realpath "$XDG_CONFIG_HOME")"
else
	export CONF_DIR="$HOME/.config"
fi

if [ -n "$XDG_DATA_HOME" ]; then
	export DATA_DIR="$(realpath "$XDG_DATA_HOME")"
else
	export DATA_DIR="$HOME/.local/share"
fi
# ]]

# set nvim's homedir
export VIMDIR="$CONF_DIR/nvim"

export ABDUCO_SOCKET_DIR="$CONF_DIR/abduco"

# tell npm to NOT try to write to system dirs if I pass it the global flag
export npm_config_prefix="$DATA_DIR/node_modules"

export ASPROOT="$DATA_DIR/asp"

export TERMINFO="$DATA_DIR/terminfo"

export LESSCHARSET='utf-8'
export LESS='--RAW-CONTROL-CHARS --chop-long-lines --incsearch --use-color --ignore-case'

if command -v moar >/dev/null; then
	export PAGER=moar
	export MOAR='-no-linenumbers'
fi

export WINEARCH="win32"
export WINEDEBUG="+err,+warn,-fixme,-trace"

export TWEEGO_PATH="$CONF_DIR/Tweego/story-formats"

# save some writes
export XBPS_MASTERDIR="/tmp/xbps-masterdir"

unset CONF_DIR DATA_DIR

source "$HOME/.profile"

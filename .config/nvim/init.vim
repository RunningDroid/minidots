if has('vim_starting')
    set nocompatible " This must be first, because it changes other options as a side effect.
endif
if has("autocmd")

  " Enable file type detection.
  " Use the default filetype settings, so that mail gets 'tw' set to 72,
  " 'cindent' is on in C files, etc.
  " Also load indent files, to automatically do language-dependent indenting.
  filetype plugin indent on
  au BufNewFile,BufRead *.svg setf svg

else

  set autoindent    " always set autoindenting on

endif " has("autocmd")

set termguicolors

" allow backspacing over everything in insert mode
set backspace=indent,eol,start

" formatting
set smarttab
set noexpandtab
set tabstop=4
set shiftwidth=0
set softtabstop=-1
set nowrap
set cursorline
set wildmenu
set lazyredraw
set showmatch
set ignorecase
set smartcase

" vim-plug stuff
" This is so I can version control my neovim config but not the plugins
if empty(glob('~/.local/share/nvim/site/autoload/plug.vim'))
  silent !curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif
" pull in the plugins
" Dependencies are shown as `Plug Dependency | Plug Dependant`
" Dependency ordering is only stylistic, all plugins are loaded simultaneously
call plug#begin('~/.local/share/nvim/plugged')

	" make vim-plug's help docs available to :help
	Plug 'junegunn/vim-plug'

	Plug 'itchyny/lightline.vim'
	Plug 'jacquesbh/vim-showmarks'
	Plug 'othree/html5.vim'
	Plug 'bogado/file-line'
	Plug 'scrooloose/nerdtree'
	Plug 'bitc/vim-bad-whitespace'
	Plug 'dense-analysis/ale'
	Plug 'mhinz/vim-startify'
	Plug 'rust-lang/rust.vim'
	Plug 'vim-scripts/iptables'
	Plug 'vim-scripts/ifdef-highlighting'
	Plug 'dsummersl/wikia-csv'
	Plug 'Shougo/deoplete.nvim'
	Plug 'cespare/vim-toml'
	Plug 'roryokane/detectindent'
	Plug 'ryanoasis/vim-devicons'
	Plug 'bluz71/vim-moonfly-colors', { 'as': 'moonfly' }
	Plug 'Shougo/neco-syntax'
	Plug 'editorconfig/editorconfig-vim'
	Plug 'nfnty/vim-nftables'
	Plug 'gpanders/editorconfig.nvim'
	Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}
	if !empty(glob('~/Sync/writeily/todo.txt'))
		Plug 'MunifTanjim/nui.nvim' | Plug 'arnarg/todotxt.nvim'
	endif
	Plug 'jamespeapen/swayconfig.vim'
" Initialize plugin system
call plug#end()

colorscheme moonfly

" lightline config
let g:lightline = { 'colorscheme': 'moonfly'}
set laststatus=3
set noshowmode

" vim-shomarks config
nnoremap ` :ShowMarksOnce<cr>`

" vim-startify config
let g:startify_session_dir = '$VIMDIR/sessions'
let g:startify_list_order = ['files', 'sessions', 'bookmarks']
"let g:startify_files_number = 10
"let g:startify_bookmarks = ['$VIMDIR/init.vim']
let g:startify_session_detection = 0
let g:startify_change_to_dir = 1
let g:startify_custom_header = [
            \ '   NeoVim',
            \ '',
            \ ]

" colors for startify
"hi StartifyHeader ctermfg=darkgreen
"hi StartifyBracket ctermfg=240
"hi StartifySpecial ctermfg=darkred
"hi StartifyNumber ctermfg=white
"hi StartifyPath ctermfg=245
"hi StartifySlash ctermfg=240
"hi StartifyFile ctermfg=blue

"deoplete stuff
let g:deoplete#enable_at_startup = 1

" set up nvim-treesitter
lua << EOF
	require'nvim-treesitter.configs'.setup {
	  -- A list of parser names, or "all" (the listed parsers should always be installed)
	  ensure_installed = { "lua", "vim", "vimdoc", "bash" },

	  -- Install parsers synchronously (only applied to `ensure_installed`)
	  sync_install = false,

	  -- Automatically install missing parsers when entering buffer
	  -- Recommendation: set to false if you don't have `tree-sitter` CLI installed locally
	  auto_install = true,

	  highlight = {
		enable = true,

		-- Setting this to true will run `:h syntax` and tree-sitter at the same time.
		-- Set this to `true` if you depend on 'syntax' being enabled (like for indentation).
		-- Using this option may slow down your editor, and you may see some duplicate highlights.
		-- Instead of true it can also be a list of languages
		additional_vim_regex_highlighting = { zsh },
	  },
	}
EOF

" NERDTree config
map <C-A-n> :NERDTreeToggle<CR>

set list " show certain non-printing chars as described by listchars
set listchars=tab:\\\x7C\ ,leadmultispace:\\\x7C\ \ \ ,conceal:\*,nbsp:\\u2420 " show indents, concealed chars, & non-breaking spaces
set history=50		" keep 50 lines of command line history
set ruler		" show the cursor position all the time
set showcmd		" display incomplete commands
set incsearch		" do incremental searching

" have treesitter define folds
set foldmethod=expr
set foldexpr=nvim_treesitter#foldexpr()

noremap <C-P> :tabprevious<CR>
noremap <C-N> :tabnext<CR>

" Switch syntax highlighting on, when the terminal has colors
if &t_Co > 2
	syntax on
	set hlsearch
endif

" Only do this part when compiled with support for autocommands.
if has("autocmd")

  " When editing a file, always jump to the last known cursor position.
  " Don't do it when the position is invalid or when inside an event handler
  " (happens when dropping a file on gvim).
  " Also don't do it when the mark is in the first line, that is the default
  " position when opening a file.
  autocmd BufReadPost *
    \ if line("'\"") > 1 && line("'\"") <= line("$") |
    \   exe "normal! g`\"" |
    \ endif

endif " has("autocmd")

" Convenient command to see the difference between the current buffer and the
" file it was loaded from, thus the changes you made.
" Only define it when not defined already.
if !exists(":DiffOrig")
  command DiffOrig vert new | set buftype=nofile | read ++edit # | 0d_ | diffthis
          \ | wincmd p | diffthis
endif

set spell   " Spell Checking

set number  " add line numbers

set backup
set writebackup " keep backups
set backupext='.bak'
set backupdir=$VIMDIR/backup

" persistent undo
set undofile
set undodir=$VIMDIR/undos

" UTF-8 ALL THE THINGS !!!!1!!one!
set fileencoding=utf-8
set encoding=utf-8
set termencoding=utf-8

" paste stuff
set pastetoggle=<F2>

lua <<EOF
function file_exists(name)
	local file = io.open(name,"r")
	if file ~= nil then
	   io.close(file)
	   return true
	else
	   return false
	end
end

-- todotxt config
todo_file_path=os.getenv('HOME').."/Sync/writeily/todo.txt"
if file_exists(todo_file_path) then
	require('todotxt-nvim').setup({
		todo_file = todo_file_path,
		sidebar = {
			width = 60
		},
	})
end
EOF

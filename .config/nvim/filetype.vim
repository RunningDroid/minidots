augroup filetypedetect
    " Mail
    autocmd BufRead,BufNewFile *mutt-*          setfiletype mail

    " tup
    autocmd BufNewFile,BufRead Tupfile,*.tup    setfiletype tup

    " csv/tsv
    autocmd BufNewFile,BufRead *.csv            setfiletype csv
    autocmd BufNewFile,BufRead *.tsv            setfiletype csv
    autocmd BufNewFile,BufRead *.tsv            Delimiter \t
    autocmd BufNewFile,BufRead *.tsv            setlocal ts=20 sw=25

augroup END

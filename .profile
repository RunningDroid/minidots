# .profile
# shellcheck shell=dash

# [[ XDG Base Directory Specification 0.8
if [ -n "$XDG_CONFIG_HOME" ]; then
	CONF_DIR="$(realpath "${XDG_CONFIG_HOME}")"
	export CONF_DIR
else
	export CONF_DIR="${HOME}/.config"
fi

if [ -n "$XDG_CACHE_HOME" ]; then
	CACHE_DIR="$(realpath "${XDG_CACHE_HOME}")"
	export CACHE_DIR
else
	export CACHE_DIR="${HOME}/.cache"
fi

if [ -n "$XDG_DATA_HOME" ]; then
	DATA_DIR="$(realpath "${XDG_DATA_HOME}")"
	export DATA_DIR
else
	export DATA_DIR="${HOME}/.local/share"
fi

if [ -n "$XDG_STATE_HOME" ]; then
	STATE_DIR="$(realpath "${XDG_STATE_HOME}")"
	export STATE_DIR
else
	export STATE_DIR="${HOME}/.local/state"
fi

if [ -z "$XDG_RUNTIME_DIR" ] && [ -e "/run/user/$(id -u)" ]; then
	XDG_RUNTIME_DIR="/run/user/$(id -u)"
	export XDG_RUNTIME_DIR
fi
# ]]

# add items to $PATH without duplicating them
# stolen from Void Linux's /etc/profile
prependpath () {
    case ":$PATH:" in
        *:"$1":*|"$1":*|*:"$1")
            ;;
        *)
            PATH="$1:${PATH:+$PATH}"
    esac
}
prependpath "${HOME}/bin"

postpendpath () {
    case ":$PATH:" in
        *:"$1":*|"$1":*|*:"$1")
            ;;
        *)
            PATH="${PATH:+$PATH}:$1"
    esac
}
postpendpath "${HOME}/.cargo/bin"
postpendpath "${DATA_DIR}/node_modules/bin"
postpendpath "${HOME}/.local/bin"
postpendpath "${HOME}/.luarocks/bin"

if command -v go >/dev/null; then
	export GOPATH="${DATA_DIR}/go"
	export GOMODCACHE="${CACHE_DIR}/go/mod"
	postpendpath "${HOME}/.local/share/go/bin"
fi

unset -f prependpath postpendpath
export PATH

export LANG='en_US.UTF-8'
export TERMINAL=foot
export EDITOR="/usr/bin/nvim"

export _JAVA_OPTIONS="-Dawt.useSystemAAFontSettings=on -Dswing.aatext=true"

export QT_QPA_PLATFORMTHEME='gtk2'

export XAUTHORITY="${CACHE_DIR}/Xauthority"

# find luarocks libs for the running Lua version
if command -v luarocks >/dev/null; then
	luarocksTree="$(luarocks config home_tree)"
	export LUA_INIT="lua_ver = string.sub(_VERSION, -3)
	luarocks_tree = '${luarocksTree}'
	package.path = string.format('%s/share/lua/%s/?.lua;$HOME/.luarocks/share/lua/%s/?/init.lua;', luarocks_tree, lua_ver, lua_ver) .. package.path
	package.cpath = string.format('%s/lib/lua/%s/?.so;', luarocks_tree, lua_ver) .. package.cpath"
fi

# let sv talk to the user daemon
export SVDIR="${DATA_DIR}/service"

# make javac actually work
export JAVA_TOOL_OPTIONS="-Dfile.encoding=UTF8"

# make cargo use sccache
if command -v sccache >/dev/null; then
	export RUSTC_WRAPPER=sccache
fi

# tell xtools(1) where my void-packages clone is
if [ -e "$HOME/git/void-packages" ]; then
	export XBPS_DISTDIR="$HOME/git/void-packages"
fi

# xdg-ninja suggested variables
# https://github.com/b3nj5m1n/xdg-ninja
# {
export CARGO_HOME="${DATA_DIR}"/cargo

export GTK2_RC_FILES="${CONF_DIR}"/gtk-2.0/gtkrc

export IPFS_PATH="${DATA_DIR}"/ipfs

export MINETEST_USER_PATH="${DATA_DIR}"/minetest

export NPM_CONFIG_INIT_MODULE="$CONF_DIR"/npm/config/npm-init.js
export NPM_CONFIG_CACHE="$CACHE_DIR"/npm
export NPM_CONFIG_TMP="$XDG_RUNTIME_DIR"/npm

export PLATFORMIO_CORE_DIR="$DATA_DIR"/platformio

export INPUTRC="$CONF_DIR"/readline/inputrc

export RUSTUP_HOME="$DATA_DIR"/rustup
#}

unset CONF_DIR DATA_DIR CACHE_DIR STATE_DIR
